package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
  
  //DB接続処理
  
  private static final String URL ="jdbc:mysql://localhost/";
  private static final String DB_NAME ="usermanagement";
  private static final String PARAMETERS ="?useUnicode=true&characterEncoding=utf8&useSSL=false";
  private static final String USER = "root";
  private static final String PASS = "password";

  //DBへ接続するコネクションを返す
  public static Connection getConnection() {
    Connection conn = null;
    try {
    Class.forName("com.mysql.jdbc.Driver");
    conn = DriverManager.getConnection(URL + DB_NAME + PARAMETERS, USER, PASS);
    
    }catch(SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    }
    return conn;
  }
  
}
