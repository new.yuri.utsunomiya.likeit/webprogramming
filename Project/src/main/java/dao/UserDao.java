package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.User;

public class UserDao {


  // ログインIDとパスワードがユーザー情報テーブルに存在するかをみるメソッド
  public User findByLoginInfo(String _loginId, String _password) {

    // コネクションをゲットするためにコネクションConnを定義
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      // SELECT文を準備する
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECT文を実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, _loginId);
      pStmt.setString(2, _password);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createTime = rs.getTimestamp("create_date");
      Timestamp updateTime = rs.getTimestamp("update_date");

      return new User(id, loginId, name, birthDate, password, isAdmin, createTime, updateTime);

    } catch (SQLException e) {
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // ログインIDがユーザー情報テーブルに存在するかをみるメソッド
  public User findById(String _loginId) {

    // コネクションをゲットするためにコネクションConnを定義
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      // SELECT文を準備する
      String sql = "SELECT * FROM user WHERE login_id = ?";

      // SELECT文を実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, _loginId);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createTime = rs.getTimestamp("create_date");
      Timestamp updateTime = rs.getTimestamp("update_date");

      return new User(id, loginId, name, birthDate, password, isAdmin, createTime, updateTime);

    } catch (SQLException e) {
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // IDがユーザー情報テーブルに存在するかをみるメソッド
  public User findById(int id) {

    // コネクションをゲットするためにコネクションConnを定義
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      // SELECT文を準備する
      String sql = "SELECT * FROM user WHERE id = ?";

      // SELECT文を実行
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      id = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createTime = rs.getTimestamp("create_date");
      Timestamp updateTime = rs.getTimestamp("update_date");

      return new User(id, loginId, name, birthDate, password, isAdmin, createTime, updateTime);

    } catch (SQLException e) {
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  // ユーザー一覧を取得するメソッド
  public List<User> findAll() {

    // まずはコネクションをゲットする
    // 初期化
    Connection conn = null;
    // List<User>を初期化s
    List<User> userList = new ArrayList<User>();
    // データベースと接続
    try {
      conn = DBManager.getConnection();
      // sql文を準備（管理者以外にする条件→whereを使うといい）
      String sql = "SELECT * FROM user WHERE is_admin = false";
      // sqlを実行し、結果表を取得
      Statement Stmt = conn.createStatement();
      ResultSet rs = Stmt.executeQuery(sql);
      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createTime = rs.getTimestamp("create_date");
        Timestamp updateTime = rs.getTimestamp("update_date");

        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createTime, updateTime);
        userList.add(user);

      }

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;

  }

  public void insert(String loginId, String name, String encodedPassword, java.sql.Date birthDate) {
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
      conn = DBManager.getConnection();
      String insertSql =
          "INSERT INTO user (login_id,password,name,birth_date,create_date,update_date)VALUES(?,?,?,?, now(), now())";
      stmt = conn.prepareStatement(insertSql);
      stmt.setString(1, loginId);
      stmt.setString(2, encodedPassword);
      stmt.setString(3, name);
      stmt.setDate(4, birthDate);
      stmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        if (stmt != null) {
          stmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();


      }
    }
  }

  public void update(int id, String name, String encodedPassword, java.sql.Date birthDate) {
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
      conn = DBManager.getConnection();
      String updateSql =
          "UPDATE user SET password=?, name=?, birth_date=?, update_date =now() WHERE id=?";
      String updateSql2 = "UPDATE user SET name=?, birth_date=?, update_date =now() WHERE id=?";



      // パスワードが空欄の時は、既存のデータを引き継ぎする（空欄のときは更新しない）
      if (!encodedPassword.equals("")) {
        stmt = conn.prepareStatement(updateSql);
        stmt.setString(1, encodedPassword);
        stmt.setString(2, name);
        stmt.setDate(3, birthDate);
        stmt.setInt(4, id);
        stmt.executeUpdate();

      } else if (encodedPassword.equals("")) {
        stmt = conn.prepareStatement(updateSql2);
        stmt.setString(1, name);
        stmt.setDate(2, birthDate);
        stmt.setInt(3, id);
        stmt.executeUpdate();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        if (stmt != null) {
          stmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }

  public void delete(String id) {
    Connection conn = null;
    PreparedStatement stmt = null;
    try {
      conn = DBManager.getConnection();
      String deleteSql = "DELETE FROM user WHERE id=?";
      stmt = conn.prepareStatement(deleteSql);
      stmt.setString(1, id);
      stmt.executeUpdate();


    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      try {
        if (stmt != null) {
          stmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }



  // 検索して該当するユーザー一覧を取得するメソッド
  public List<User> search(String loginId, String name, String birthDateStart,
      String birthDateEnd) {

    // まずはコネクションをゲットする
    // 初期化
    Connection conn = null;
    // List<User>を初期化s
    List<User> userSearchList = new ArrayList<User>();
    // データベースと接続
    try {

      conn = DBManager.getConnection();
      // sql文を準備
      String searchSql = "SELECT * FROM user WHERE is_admin = false"; // 共通部分のみ宣言
      StringBuilder stringBuilder = new StringBuilder(searchSql); // String builder

      // 初期値を設定
      PreparedStatement pStmt = null;

      // 条件に応じて実行するSQL文をセット
      if (loginId.equals("") && name.equals("") && birthDateStart.equals("") && birthDateEnd.equals("")) { // 全部空欄
        stringBuilder.toString();
        // sqlを実行し、結果表を取得
        pStmt = conn.prepareStatement(searchSql);

      } else if (!loginId.equals("") && name.equals("") && birthDateStart.equals("") && birthDateEnd.equals("")) { // loginIdのみ記入
        searchSql = (stringBuilder.append(" AND login_id = ?")).toString();
        // sqlを実行し、結果表を取得
        pStmt = conn.prepareStatement(searchSql);
        // 検索欄への記入内容をSQL文にセット
        pStmt.setString(1, loginId);
      } else if (loginId.equals("") && !name.equals("") && birthDateStart.equals("") && birthDateEnd.equals("")) { // nameのみ記入
        searchSql = (stringBuilder.append(" AND name LIKE ?")).toString();
        // sqlを実行し、結果表を取得
        pStmt = conn.prepareStatement(searchSql);
        // 検索欄への記入内容をSQL文にセット
        pStmt.setString(1, "%" + name + "%");
      } else if (loginId.equals("") && name.equals("") && !birthDateStart.equals("") && birthDateEnd.equals("")) { // birthDateStartのみ記入
        searchSql = (stringBuilder.append(" AND birth_date >= ?")).toString();
        // sqlを実行し、結果表を取得
        pStmt = conn.prepareStatement(searchSql);
        // 検索欄への記入内容をSQL文にセット
        pStmt.setString(1, birthDateStart);
      } else if (loginId.equals("") && name.equals("") && birthDateStart.equals("") && !birthDateEnd.equals("")) { // birthDateEndのみ記入
        searchSql = (stringBuilder.append(" AND birth_date <= ?")).toString();
        // sqlを実行し、結果表を取得
        pStmt = conn.prepareStatement(searchSql);
        // 検索欄への記入内容をSQL文にセット
        pStmt.setString(1, birthDateEnd);
      } else if (loginId.equals("") && name.equals("") && !birthDateStart.equals("") && !birthDateEnd.equals("")) { // 日付範囲指定のみ記入
        searchSql = (stringBuilder.append(" AND ? <= birth_date AND birth_date <= ?")).toString();
        // sqlを実行し、結果表を取得
        pStmt = conn.prepareStatement(searchSql);
        // 検索欄への記入内容をSQL文にセット
        pStmt.setString(1, birthDateStart);
        pStmt.setString(2, birthDateEnd);

      } else if (!loginId.equals("") && !name.equals("") && !birthDateStart.equals("") && !birthDateEnd.equals("")) { // 全部記入
        searchSql = (stringBuilder.append(" AND login_id = ? AND name LIKE ? AND ? <= birth_date AND birth_date <= ?")).toString();
        // sqlを実行し、結果表を取得
        pStmt = conn.prepareStatement(searchSql);
        // 検索欄への記入内容をSQL文にセット
        pStmt.setString(1, loginId);
        pStmt.setString(2, "%" + name + "%");
        pStmt.setString(3, birthDateStart);
        pStmt.setString(4, birthDateEnd);
      }


      ResultSet rs = pStmt.executeQuery();
      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId1 = rs.getString("login_id");
        String name1 = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createTime = rs.getTimestamp("create_date");
        Timestamp updateTime = rs.getTimestamp("update_date");

        User user =
            new User(id, loginId1, name1, birthDate, password, isAdmin, createTime, updateTime);
        userSearchList.add(user);

      }

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userSearchList;

  }
}
