package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncoder;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserUpdateServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // ログインチェック
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // もしユーザー情報が取得できない場合リダイレクト
    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    // idをキーにパラメータを取得
    String strId = request.getParameter("id");
    int id = Integer.valueOf(strId);

    // idを引数にUserDao.findById()を呼び出す
    UserDao userDao = new UserDao();
    User userDetail = userDao.findById(id);

    // キーuserに対して上記の戻り値をリクエストスコープに対してセットする
    request.setAttribute("user", userDetail);

    // userDetail.jspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    try {
      // 文字コードを設定
      request.setCharacterEncoding("UTF-8");
      // 記入され送信されたリクエストパラメータを取得（パスワード、パスワード確認、ユーザー名、生年月日のみ）
      String strId = request.getParameter("strId");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("passwordConfirm");
      String name = request.getParameter("name");
      String strBirthDate = request.getParameter("birthDate");

      // 全ての値が入力されたかどうか（パスワードは空欄でも良い）
      boolean isNameEmpty = (name.equals(""));
      // 生年月日が空欄
      boolean isBirthDateEmpty = strBirthDate.equals("");
      // パスワードが2つとも同じか
      boolean isPasswordSame = (password.equals(passwordConfirm));


      // ひとつでも該当する場合(エラー)は入力された値を画面に表示するためリクエストコープへセット（pw以外）
      if (isNameEmpty || isBirthDateEmpty || !isPasswordSame) {

        int id = Integer.valueOf(strId);

        // idを引数にUserDao.findById()を呼び出す
        UserDao userDao = new UserDao();
        User userDetail = userDao.findById(id);
        userDetail.setName(name);

        // 生年月日入れても名前/passが空欄だった場合も、生年月日はnullにはせずに生かしたい
        if (!isBirthDateEmpty) {
          Date birthDate = Date.valueOf(strBirthDate);
          userDetail.setBirth_date(birthDate);
        } else {
          userDetail.setBirth_date(null);
        }

        // キーuserに対して上記の戻り値をリクエストスコープに対してセットする
        request.setAttribute("user", userDetail);

        // リクエストスコープにエラーメッセージをセット→setAttribute
        request.setAttribute("errMsg", "入力した内容は正しくありません");

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
        dispatcher.forward(request, response);
        return;

      }

      UserDao userDao = new UserDao(); // インスタンス化
      Date birthDate = Date.valueOf(strBirthDate);
      int id = Integer.parseInt(strId);
      // パスの暗号化
      String encodedPassword = PasswordEncoder.encodePassword(password);
      userDao.update(id, name, encodedPassword, birthDate);

      response.sendRedirect("UserListServlet");
    } catch (IllegalArgumentException e) {
      e.printStackTrace();

    }
  }
}
