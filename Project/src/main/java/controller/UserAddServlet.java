package controller;

import java.io.IOException;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncoder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAddServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // ログインチェック
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // もしユーザー情報が取得できない場合リダイレクト
    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }
    // ユーザー情報が取得できる場合フォワードする
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // 文字コードを設定
    request.setCharacterEncoding("UTF-8");
    // 記入され送信されたリクエストパラメータを取得
    String loginId = request.getParameter("loginiId");
    String password = request.getParameter("password");
    String passwordConfirm = request.getParameter("passwordConfirm");
    String name = request.getParameter("name");
    String strBirthDate = request.getParameter("birthDate");

    
    // すでに使われているIDかどうかID未使用チェック
    UserDao userDao = new UserDao();
    User user = userDao.findById(loginId);
    // 全ての値が入力されたかどうか
    boolean isEmpty = (loginId.equals("") || password.equals("") || passwordConfirm.equals("")
        || name.equals("") || strBirthDate.equals(""));
    // パスワードが2つとも同じか
    boolean isPasswordSame = (password.equals(passwordConfirm));

    // ひとつでも該当する場合は入力された値を画面に表示するためリクエストコープへセット（pw以外）
    if (isEmpty || !isPasswordSame) {
      request.setAttribute("loginId", loginId);
      request.setAttribute("name", name);
      request.setAttribute("birthDate", strBirthDate);
     
      
      // リクエストスコープにエラーメッセージをセット→setAttribute
      request.setAttribute("errMsg", "入力した内容は正しくありません");


      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
      return;
    }

    try {
      Date birthDate = Date.valueOf(strBirthDate);
      
      //パスの暗号化
      String encodedPassword = PasswordEncoder.encodePassword(password);
      
      //DBへ書き込み
      userDao.insert(loginId, name, encodedPassword, birthDate);
      
      
      
      response.sendRedirect("UserListServlet");
      
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
    }

  }
}
