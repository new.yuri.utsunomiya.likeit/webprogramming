package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserListServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // ログインチェック
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // もしユーザー情報が取得できない場合リダイレクト
    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }


    // ユーザー一覧表を表示するため情報を呼び出すメソッドを実行
    UserDao userDao = new UserDao(); // 新規インスタンス
    List<User> userList = userDao.findAll(); // 格納

    // 戻り値をリクエストスコープにセット
    request.setAttribute("userList", userList);

    // userListへフォワードする
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
 
    // 文字コードを設定
    request.setCharacterEncoding("UTF-8");
    // 記入され送信されたリクエストパラメータを取得
    String loginId = request.getParameter("loginId");
    String name = request.getParameter("name");
    String birthDateStart = request.getParameter("birthDateStart");
    String birthDateEnd = request.getParameter("birthDateEnd");
    
    // ユーザー一覧表を表示するため情報を呼び出すメソッドを実行
    UserDao userDao = new UserDao(); // 新規インスタンス
    
    List<User> userSearchList = userDao.search(loginId, name, birthDateStart, birthDateEnd); // 格納

    // 戻り値をリクエストスコープにセット
    request.setAttribute("userList", userSearchList);

    // userListへフォワードする
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
    dispatcher.forward(request, response);

  }

}
