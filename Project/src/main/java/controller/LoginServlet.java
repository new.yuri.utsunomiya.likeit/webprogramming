package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncoder;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public LoginServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // userLogin.jspへフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
    dispatcher.forward(request, response);

  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // リクエストパラメータの文字コードを指定
    request.setCharacterEncoding("utf-8");

    // リクエストパラメータの入力項目を取得
    String loginId = request.getParameter("loginId");
    String password = request.getParameter("password");

    // リクエストパラメータの入力項目を引数に渡してfindByLoginInfo()を実行
    // UserDaoのインスタンスを作成
    UserDao userDao = new UserDao();
    //パスの暗号化
    String encodedPassword = PasswordEncoder.encodePassword(password);
    // メソッドの実行
    User user = userDao.findByLoginInfo(loginId, encodedPassword);

    // もし該当のユーザーが見つからなかった場合
    if (user == null) {
      // loginIdをキーに入力されたログインIDを保存する
      request.setAttribute("loginId", loginId);
      // リクエストスコープにエラーメッセージをセット→setAttribute
      request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
      // userLoginへフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
      dispatcher.forward(request, response);

      return;
    }

    // もし該当のユーザーが見つかった場合
    // セッションスコープにユーザーの情報をセット
    // HttpSessionインターフェースを用いて入力されたID情報を呼び出す
    HttpSession session = request.getSession();
    // 呼び出した値をユーザー情報としてセット
    session.setAttribute("userInfo", user);
    // UserListサーブレットにリダイレクトで遷移
    response.sendRedirect("UserListServlet");

  }

}
