package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserDeleteServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // ログインチェック
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // もしユーザー情報が取得できない場合リダイレクト
    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    // idをキーにパラメータを取得
    String strId = request.getParameter("id");
    int id = Integer.valueOf(strId);


    // idを引数にUserDao.findById()を呼び出す
    UserDao userDao = new UserDao();
    User userDelete = userDao.findById(id);

    // キーuserに対して上記の戻り値をリクエストスコープに対してセットする
    request.setAttribute("user", userDelete);

    // userDelete.jspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
    dispatcher.forward(request, response);
    }
  
  


  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    try {
      // 文字コードを設定
      request.setCharacterEncoding("UTF-8");
      // 送信されたリクエストパラメータを取得（文字列のIDのみでよい）
      String deleteId = request.getParameter("id");
     
      
      UserDao userDao2 = new UserDao(); // インスタンス化
      userDao2.delete(deleteId);
      
      response.sendRedirect("UserListServlet");
    } catch (IllegalArgumentException e) {
      e.printStackTrace();
      
    }

}
}
