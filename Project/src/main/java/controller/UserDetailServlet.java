package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserDetailServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // ログインチェック
    HttpSession session = request.getSession();
    User user = (User) session.getAttribute("userInfo");

    // もしユーザー情報が取得できない場合リダイレクト
    if (user == null) {
      response.sendRedirect("LoginServlet");
      return;
    }

    //idをキーにパラメータを取得
    String strId = request.getParameter("id");
    int id = Integer.valueOf(strId);
    
    //idを引数にUserDao.findById()を呼び出す
    UserDao userDao = new UserDao();
    User userDetail = userDao.findById(id);
    
    //キーuserに対して上記の戻り値をリクエストスコープに対してセットする
    request.setAttribute("user", userDetail);
    
    //userDetail.jspにフォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
    dispatcher.forward(request, response);
  }



  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

  }

}
