<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="ja">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ユーザ削除画面</title>
	<!-- BootstrapのCSS読み込み -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
		integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>

	<!-- header -->
	<header>
		<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row justify-content-end">

			<ul class="navbar-nav flex-row">
				<li class="nav-item"><span class="navbar-text">${userInfo.name }
						さん</span>
				</li>
				<li class="nav-item"><a class="nav-link text-danger" href="LogoutServlet">ログアウト</a></li>
			</ul>
		</nav>
	</header>
	<!-- /header -->

	<!-- body -->
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3">
				<div class="row mb-5 mt-3">
					<div class="col">
						<h1 class="text-center">ユーザ削除確認</h1>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<p>ログインID： ${user.login_id}</p>
						<p>を本当に削除してよろしいでしょうか？</p>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<a href="UserListServlet" class="btn btn-light btn-block">いいえ</a>
					</div>
					<div class="col">
					<form action="UserDeleteServlet" method="post" >
					<button type="submit" class="btn btn-primary btn-block">はい</button>
						<input type="hidden" id="id" value="${user.id }" name="id" />
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>

</html>