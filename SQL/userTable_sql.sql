CREATE DATABASE usermanagement DEFAULT CHARACTER SET UTF8;
USE usermanagement;
CREATE TABLE user(
id SERIAL PRIMARY KEY UNIQUE NOT NULL,
login_id VARCHAR(255) NOT NULL, 
name VARCHAR(255) NOT NULL, 
birth_date DATE NOT NULL, password VARCHAR(255) NOT NULL,  
is_admin boolean NOT NULL DEFAULT 0, 
create_date DATETIME NOT NULL, 
update_date DATETIME NOT NULL
);
INSERT INTO user(
login_id,
name,
birth_date,
password,
is_admin,
create_date,
update_date
)
VALUES(
'admin', 
'管理者', 
'1996-02-29', 
'password', 
1, 
now(), 
now()
);
